const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");

const markdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");
let markdownItOptions = {
  html: true,
};
const pluginTOC = require("eleventy-plugin-toc");

module.exports = function (eleventyConfig) {
  eleventyConfig.setUseGitIgnore(false);
  eleventyConfig.addPassthroughCopy({
    "./_assets/main.generated.css": "./_assets/css/main.css",
  });
  eleventyConfig.addPassthroughCopy({
    "./node_modules/prism-themes/themes/prism-holi-theme.css":
      "./_assets/css/theme.css",
  });
  eleventyConfig.addPassthroughCopy("./_assets/img");
  eleventyConfig.addPassthroughCopy("./_assets/js");

  eleventyConfig.addPlugin(syntaxHighlight);

  eleventyConfig.setLibrary(
    "md",
    markdownIt(markdownItOptions).use(markdownItAnchor)
  );
  eleventyConfig.addPlugin(pluginTOC);

  const md = new markdownIt({
    html: true,
  });

  eleventyConfig.addFilter("markdown", (content) => {
    return md.renderInline(content);
  });
};
