// check if navbar has been stickied
const el = document.querySelector(".navbar");
const observer = new IntersectionObserver(
  ([e]) => e.target.classList.toggle("stickied", e.intersectionRatio < 1),
  { threshold: [1] }
);

observer.observe(el);

const menuOpenBtn = document.querySelector(".menu-open-btn");
const menuCloseBtn = document.querySelector(".menu-close-btn");
const offcanvasMenu = document.querySelector(".offcanvas-menu");

function closeTOC() {
  offcanvasMenu.classList.remove("translate-x-0");
  offcanvasMenu.classList.add("translate-x-full");
  let backdrop = document.querySelector(".offcanvas-backdrop");
  if (backdrop) {
    backdrop.remove();
  }
}

const tocLinks = document.querySelectorAll(".table-of-contents li a");
tocLinks.forEach((link) => {
  link.addEventListener("click", () => {
    closeTOC();
  });
});

function createCopyCodeButton() {
  let copyCodeButton = document.createElement("button");
  copyCodeButton.classList.add("btn-copy-code");
  copyCodeButton.textContent = "Copy";
  return copyCodeButton;
}

async function copyCode(codeBlock, copyCodeButton) {
  // copy code
  let code = codeBlock.querySelector("code");
  let text = code.textContent;

  await navigator.clipboard.writeText(text);

  // change button text briefly for confirmation
  copyCodeButton.textContent = "Copied!";
  setTimeout(() => {
    copyCodeButton.textContent = "Copy";
  }, 2000);
}

document.addEventListener("DOMContentLoaded", (event) => {
  // wrap each section in a section wrapper
  let main = document.querySelector(".content-container");
  if (main == null) {
    return;
  }
  document.querySelectorAll(".content-container > *").forEach((el) => {
    if (el == main.firstChild) {
      // do nothing
    } else if (["H2"].indexOf(el.tagName) >= 0) {
      section = document.createElement("section");
      section.id = el.id;
      el.insertAdjacentElement("beforebegin", section);
      section.appendChild(el);
      el.removeAttribute("id");
    } else {
      section.appendChild(el);
    }
  });

  // setting first toc element as active
  let firstTocElement = document.querySelectorAll(
    ".table-of-contents li > a"
  )[0];
  firstTocElement.setAttribute("class", "active");

  // implementing scrollspy
  const sections = document.querySelectorAll(".content-container section");
  let activeSectionId = "";

  const observer = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          document
            .querySelector(".table-of-contents li a.active")
            .classList.remove("active");
          activeSectionId = entry.target.id;
          document
            .querySelector(
              `.table-of-contents ol li a[href="#${activeSectionId}"]`
            )
            .classList.add("active");
        }
      });
    },
    {
      root: document,
      rootMargin: "-10% 0px -90% 0px",
    }
  );

  sections.forEach((section) => {
    observer.observe(section);
  });

  // copy code and customize code blocks implementation
  // ----
  // adding ability for user to tab into code blocks that overflow
  // and be able to scroll right with right arrow
  let allCodeBlocks = document.querySelectorAll("pre");
  allCodeBlocks.forEach((codeBlock) => {
    codeBlock.setAttribute("tabindex", 0);
  });

  // add a header to code blocks
  allCodeBlocks.forEach((codeBlock) => {
    // wrap pre blocks in a parent div container
    let parentContainer = document.createElement("div");
    let parent = codeBlock.parentNode;
    parent.insertBefore(parentContainer, codeBlock);
    parentContainer.appendChild(codeBlock);
    parentContainer.classList.add("code-block-container");

    let language = codeBlock.className.split("-")[1].trim();

    // add code block header and copy code button
    if (language != "console" && language != "txt") {
      let codeBlockHeader = document.createElement("div");
      codeBlockHeader.classList.add("code-block-header");
      parentContainer.insertBefore(codeBlockHeader, parentContainer.firstChild);

      // add copy code button
      let copyCodeButton = createCopyCodeButton();
      codeBlockHeader.appendChild(copyCodeButton);

      copyCodeButton.addEventListener("click", async () => {
        await copyCode(codeBlock, copyCodeButton);
      });
    }
  });
});

menuOpenBtn.addEventListener("click", function () {
  offcanvasMenu.classList.remove("translate-x-full");
  offcanvasMenu.classList.add("translate-x-0");

  let main = document.querySelector("main");
  let backdrop = document.createElement("div");
  backdrop.className = "offcanvas-backdrop";

  let parent = main.parentNode;
  parent.insertBefore(backdrop, main);

  backdrop.addEventListener("click", () => {
    offcanvasMenu.classList.remove("translate-x-0");
    offcanvasMenu.classList.add("translate-x-full");
    backdrop.remove();
  });
});

menuCloseBtn.addEventListener("click", function () {
  closeTOC();
});
