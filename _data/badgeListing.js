const EleventyFetch = require("@11ty/eleventy-fetch");

module.exports = async function () {
  let url = "https://brettops.gitlab.io/tools/badgie/badges.json";
  return EleventyFetch(url, {
    duration: "1d", // save for 1 day
    type: "json",
  });
};
