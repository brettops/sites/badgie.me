MAKEFLAGS += -j

.PHONY: tailwindcss eleventy open
all: tailwindcss eleventy

tailwindcss:
	npx tailwindcss -i ./_assets/main.css -o ./_assets/main.generated.css --watch

eleventy:
	npx @11ty/eleventy --serve
open:
	explorer.exe .
