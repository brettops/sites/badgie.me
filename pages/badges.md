---
title: Badges
layout: badges.html
permalink: badges/
---

## What does Badgie know about?

This page contains all badges that Badgie knows about. You can expect it to get
longer over time.

If you want to add more badges, [fork the
repo](https://gitlab.com/brettops/tools/badgie) and get badgin'!
