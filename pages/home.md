---
layout: home.html
permalink: /
---

## What is Badgie?

Badgie adds badges to your project. 'Nuff said!

## How to Badgie

Install Badgie:

```bash
pip install badgie
```

Add Badgie tags:

```md
<!-- BADGIE TIME -->
<!-- END BADGIE TIME -->
```

Run Badgie:

```bash
badgie -w README.md
```

Listen to what Badgie has to say:

```console
$ badgie -w README.md
Hi, I'm Badgie! Let's add some badges! 🐦

- adding a pre-commit-enabled badge
- adding a brettops-tool badge
- adding a gitlab-latest-release badge
- adding a gitlab-coverage-report badge
- adding a gitlab-pipeline-status badge
- adding a code-style-black badge
- adding a code-style-prettier badge

That's like 7 badges! Good job! 🐦
```

Check your README. It's now overrun with badges. Too many badges to handle:

```md
<!-- BADGIE TIME -->

[![brettops tool](https://img.shields.io/badge/brettops-tool-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/tools/badgie?branch=main)](https://gitlab.com/brettops/tools/badgie/-/commits/main)
[![coverage report](https://img.shields.io/gitlab/pipeline-coverage/brettops/tools/badgie?branch=main)](https://gitlab.com/brettops/tools/badgie/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/brettops/tools/badgie)](https://gitlab.com/brettops/tools/badgie/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: black](https://img.shields.io/badge/code_style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->
```

They look like this:

[![brettops tool](https://img.shields.io/badge/brettops-tool-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/tools/badgie?branch=main)](https://gitlab.com/brettops/tools/badgie/-/commits/main)
[![coverage report](https://img.shields.io/gitlab/pipeline-coverage/brettops/tools/badgie?branch=main)](https://gitlab.com/brettops/tools/badgie/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/brettops/tools/badgie)](https://gitlab.com/brettops/tools/badgie/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: black](https://img.shields.io/badge/code_style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

You just got badged by Badgie!

## Why Badgie?

Adding badges to repositories is frustrating. It takes so much fiddling to get
them set up, and you have to do it over and over again for every repository you
create. Because of this, many projects don't have badges and probably never
will.

The thing is, the whole internet WANTS you to have badges:

- Badges let you know what's going on with a project at a glance,

- They let others know that you care about your project, and

- They just look awesome!

Don't worry. You don't have to live without badges anymore. Now there's Badgie!

## How does Badgie work?

Badgie is different because Badgie doesn't like asking for anything.

Starting from your project's working directory, Badgie attempts to reconstruct
what your repository is, where it came from, and what information is interesting
about it:

- Badgie sifts through files, URLs, and anything else, and returns a list of
  findings.

- Badgie matches findings to badges it knows about, and returns badges unique to
  your project.

- Badgie adds more badges as your project grows or Badgie learns more.

- Badgie also keeps existing badges fresh!

## What badges does Badgie badge?

Visit the [Badges]({{ '/badges/' | url }}) page to see all the badges Badgie
knows about.

## Pre-commit hook

Badgie has a pre-commit hook, so you can get fresh badges on every commit.

Add the following to a `.pre-commit-config.yaml` file. Note the empty
`rev` tag:

```yaml
repos:
  - repo: https://gitlab.com/brettops/tools/badgie
    rev: ""
    hooks:
      - id: badgie
```

Run `pre-commit autoupdate` to pin to the latest version:

```bash
pre-commit autoupdate
```

Run `pre-commit` directly or install as a hook:

```bash
# directly
pre-commit

# as a Git hook
pre-commit install
git commit -m "..."
```

## Customizations

### Badge style

Badgie keeps up with the trends and knows that not everyone likes the `flat`
style. Badgie supports any style supported by [Shields.io](https://shields.io/).
Simply pass the `-s` / `--style` option when calling Badgie:

```bash
badgie -w README.md -s for-the-badge
```

Now your badges are more different:

[![brettops tool](https://img.shields.io/badge/brettops-tool-209cdf?labelColor=162d50&style=for-the-badge)](https://brettops.io)
[![pipeline status](https://img.shields.io/gitlab/pipeline-status/brettops/tools/badgie?branch=main&style=for-the-badge)](https://gitlab.com/brettops/tools/badgie/-/commits/main)
[![coverage report](https://img.shields.io/gitlab/pipeline-coverage/brettops/tools/badgie?branch=main&style=for-the-badge)](https://gitlab.com/brettops/tools/badgie/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/brettops/tools/badgie?style=for-the-badge)](https://gitlab.com/brettops/tools/badgie/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&style=for-the-badge)](https://github.com/pre-commit/pre-commit)
[![code style: black](https://img.shields.io/badge/code_style-black-000000.svg?style=for-the-badge)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=for-the-badge)](https://github.com/prettier/prettier)

Thanks, Badgie!

## Badgie caveats

Badgie makes decisions on the assumption that you do sensible things with your
repository structure. It does not try to work around bad practices.

## Contributing

Visit the [GitLab repo](https://gitlab.com/brettops/tools/badgie).

## Credits

Badgie would not be possible without [Shields.io](https://shields.io/) and their
high quality, omnipresent, infinitely customizable badges.

Badgie's file detection code was inspired by the
[identify](https://github.com/pre-commit/identify) library.
