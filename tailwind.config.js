/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./*.{html,js,md}",
    "./_includes/**/*.{html,js,md}",
    "./pages/**/*.{html,js,md}",
  ],
  safelist: ["translate-x-full", "translate-x-0"],
  theme: {
    extend: {
      colors: {
        "badgie-blue": "#006CA5",
        "badgie-blue-darker-100": "#0A508A",
        "badgie-blue-darker-200": "#082E5E",
        "badgie-blue-darker-250": "#09264B",
        "badgie-blue-darker-300": "#0A1D38",
        "badgie-blue-darker-400": "#061228",
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
